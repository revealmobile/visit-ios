# Visit SDK

## About

The Visit SDK is a highly optimized location update and beacon scanning library for iOS. The Visit SDK uses a proprietary algorithm that provides your application with precise location data as well as the likely movement mode of the user/device - each location update tells you whether the user is currently stationary, walking, running, or driving in a city or on a highway. Visit also uses this information to intelligently utilize system resources, creating the lowest possible impact on your app's battery consumption. By default, Visit only scans for beacons once every time the user is determined to be stationary, but we provide you with all the tools you need to scan for beacons more regularly - or not at all.

The Visit SDK requires no API integrations, but for your convenience the SDK includes the class `VSTNetworkService`, which handles all of the network tasks associated with a server integration and is a also a ready-made implementation of `VSTDelegate`.

Please note that if you intend to use any information obtained through the Visit SDK for ad targeting, you _MUST_ make this use explicit when requesting location and/or bluetooth permission from the user. If you fail to disclose this information your app may be rejected or removed from the App Store.

# Visit Integration Guide

## Installation
### CocoaPods
Visit is available through CocoaPods. To install it, simply add the following line to your Podfile:

`pod "Visit"`

Because Visit contains precompiled components, you must make sure that these components are linked properly. To make sure this is the case, after each `pod install` or `pod update` you must:
1. Select the `Pods` project in the Project Navigator in your workspace.
2. Select the `Visit` target.
3. Open the `Build Phases` tab.
4. Expand `Link Binary with Libraries`.
5. Add `VSTCore.framework` by dragging it from the Project Navigator into the list (Pods/Visit/Frameworks).

Additionally, because of these precompiled components, your project must be using the version of Swift that was used to compile these components. As of this version of Visit (1.0.0) VSTCore is compiled with Swift `5.2`.

### Manual Installation
Visit can be added as a framework. First, download the iOS Framework here (`Link Coming Soon`). After adding Visit.framework to your project, you need to link your project against the following system frameworks (CoreLocation, CoreBluetooth, AdSupport).

If your project has Bitcode enabled (which is mandatory on tvOS and iOS), then you need to add a new Run Script build phase to your target with the following script:

`“$BUILT_PRODUCTS_DIR/$FRAMEWORKS_FOLDER_PATH/Visit.framework/fix-framework.sh”`

Make sure this script is run after the existing Embed Frameworks build phase.

Calling this script is required for your App Store submissions to pass validation when using Bitcode. While optional for non-Bitcode submissions, you may still add this script as it will somewhat reduce the size of the framework embedded in your app.

## Project setup
Because the Visit SDK uses iOS Location Services, it is important to communicate clearly to the end user why your application asks for their location. Make sure that your `Info.plist` file contains all of the necessary keys. You must provide a string for the `NSLocationAlwaysUsageDescription` key and/or a string for the  `NSLocationWhenInUseUsageDescription` key that clearly state why your application needs to use these services. For the Visit SDK to function in the background, you must also include in your `Info.plist` file an array for the `UIBackgroundModes` key that contains the strings `"location"` and `"bluetooth-central"`.

If your app is going to use the Visit SDK for beacon scanning, in iOS 13 you must include a value for the `NSBluetoothPeripheralUsageDescription` key in your `Info.plist` file that clearly states why your application needs this permission.

If you intend to use any information obtained through the Visit SDK for ad targeting, you _MUST_ make this use explicit in the strings that you provide for the `NSLocationAlwaysUsageDescription`, `NSLocationWhenInUseUsageDescription`, and `NSBluetoothPeripheralUsageDescription` keys in your `Info.plist`. If you fail to disclose this information your app may be rejected or removed from the App Store.

## Implementation

In the `application(_:didFinishLaunchingWithOptions:)` function of your `AppDelegate`, provide the Visit singleton with an implementation of `VSTDelegate` and then invoke the `start(launchOptions:onUserLaunch:)` function on the Visit singleton. The first argument of the `start` function, `launchOptions`, should be taken directly from the arguments passed into `application(_:didFinishLaunchingWithOptions:)`. The second argument, `onUserLaunch`, should be a function containing all of the setup you would normally place directly in the `application(_:didFinishLaunchingWithOptions:)` function of your `AppDelegate`.
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

  Visit.shared.delegate = MyVSTDelegate()
  Visit.shared.start(launchOptions: launchOptions, onUserLaunch: { options in
    /*
    ** Perform whatever logic/setup you would normally place directly in this AppDelegate function
    ** (e.g. setup CoreData, ping a network, initialize other libraries, etc...)
    */
  })
  return true
}
```
The only code executed directly in the `application(_:didFinishLaunchingWithOptions:)` function of your `AppDelegate` should be providing visit with a delegate and invoking the start function. For more information on why this is necessary, please refer to the section below.

## Starting the SDK/Handling Location Events from a Terminated State

Initializing the Visit SDK is a bit different from initializing other SDKs because the Visit SDK utilizes the iOS CoreLocation Framework's ability to launch your app in the background, from a terminated state, in order to respond to significant location updates. If you include all of the above keys in your `Info.plist` file and the user grants `.authorizedAlways` location service permission, this behavior will happen by default.

When the system launches your App in the background to handle a significant location update the user is unaware of this launch and does not see your App in the Task Manager, so it is imperative to use the absolute minimum possible resources to respond to such an update. In this situation it would be an egregious waste of system resources to initialize `CoreData`, invoke network calls, perform complex UI logic, initialize an analytics library, or perform any other common "didFinishLaunching" tasks. This is why Visit allows you to provide all of this logic in the `onUserLaunch` argument of the `start` function.

The `launchOptions` argument of `start(launchOptions:onUserLaunch:)` allows Visit to determine the conditions of the App launch. If a user triggered the launch of your app, Visit will execute your `onUserLaunch` function asynchronously on the main thread before performing any of its own setup. If the system has launched your app in the background to handle a significant location update, Visit will activate only those features necessary to respond to the location update, and when the application becomes active (e.g. the user opens it by tapping the app icon) Visit will execute your `onUserLaunch` function asynchronously on the main thread and then turn on the rest of its features. Because Visit will always execute your `onUserLaunch` function in the main thread, it is safe to perform any UI tasks you would normally execute in the `application(_:didFinishLaunchingWithOptions:)` function of your `AppDelegate`.

## `VSTDelegate`
### Overview

`VSTDelegate` is a `protocol` defined by the Visit SDK that provides the the SDK with behaviors to execute when starting or restarting the SDK, when it is time to process a batch of Locations and/or Beacons, as well as a number of optional hooks into different events within the SDK. `VSTDelegate` requires the implementation of two functions: `getConfiguration(success:failure:)` and `process(location:mode:)`. For beacon scanning to function, you must also provide an implementation of `process(beacons:location:)`. For your convenience, the Visit SDK includes an implementation of `VSTDelegate` called `VSTNetworkService` which is detailed in the next section.

### Required Members

#### `getConfiguration`

The `VSTDelegate` member `getConfiguration(success:@escaping([String:Any]) -> Void, failure:@escaping(Error?) -> Void)` is invoked when the SDK starts or restarts. This function is where you can provide Visit with configuration values to customize certain aspects of how the SDK functions. It is designed to allow clients to retrieve their config values asynchronously. Invoking the `success` argument provides the Visit SDK with a configuration, invoking the `failure` argument tells the Visit SDK that no configuration is currently available.

All viable keys for the configuration dictionary are available through the `struct` `Visit.configKeys`. They are all optional - if you omit any, Visit has default values that it will use when no other values are provided. So if you want to simply use the default configuration for the Visit SDK, your implementation of `getConfiguration` would look like this:
```
func getConfiguration(success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
  success([:])
}
```

#### `process(location:mode:)`

The `VSTDelegate` member `process(_ location: CLLocation, with mode: VSTMovementMode)` is invoked whenever the Visit SDK acquires a quality location update. The `location` argument is a standard iOS `CLLocation` object, and the `mode` argument represents the likely movement mode of the device based on Visit's proprietary algorithm.

### Optional Members

#### `process(beacons:location:)`

`process(_ beacons: Set<VSTBeacon>, at location: CLLocation)` is invoked whenever a beacon scan is completed. The argument `beacons` contains a set of a all beacons discovered during the most recent scan, and the argument `location` is the standard iOS `CLLocation` representing the latest location available when the beacon scan started. *Beacon Scanning will not happen if this member is not implemented.*

_**For more information about controlling beacon scanning, see the section_ **Additional Control Over Beacon Scanning**

#### `processTerminatedLocations(locations)`
`processTerminatedLocations(_ locations: [VSTLocation])` is invoked when your app is brought to the foreground after receiving location updates from a terminated state. The model for locations passed into this function is slightly different, but contains all the same information as `CLLocation`. If you do no implement this function, *terminated state location updating will still happen*, but nothing will be done with these location updates. To turn off terminated state location updating, provide the Visit singleton's member `shouldLaunchInBackgroundOverride` with a value of `false` before invoking the `start` function in your `AppDelegate`.


#### `movementModeDidChange(mode:)`

`movementModeDidChange(to mode: VSTMovementMode)` is invoked whenever the Movement Mode changes.

#### `didSkipLocation(location:)`

`didSkipLocation(_ location: CLLocation)` is invoked in the edge case where the Visit algorithm deems a location update inadequate. This may be due to a number of factors, but typically will only ever happen when starting up location services.

### Use

Once you have defined your implementation of `VSTDelegate`, make sure to set it to the `delegate` member of the Visit singleton before invoking `start()`, just like this:
```
Visit.shared.delegate = MyVSTDelegate()
Visit.shared.start(launchOptions: launchOptions, onUserLaunch: { options in
  /*
  ** Launch Logic
  */
})
```

## Additional Control Over Beacon Scanning

In addition to omitting  `process(beacons:location:)` from your implementation of `VSTDelegate` to turn off Beacon scanning completely, Visit offers additional fine grain control over Beacon scanning in the following members of `Visit.shared`.

`canScanForBLEBeaconsAutomatically` - If false, BLE Beacon scans will not occur automatically when the device becomes stationary. You may still manually trigger scans.

`canScanForIBeaconsAutomatically` - If false, iBeacon scans will not occur automatically when the device becomes stationary. You may still manually trigger scans.

`canRequestBLEPermissionAutomatically` If true, the first time Visit wants to scan for beacons, it will automatically ask the user for Bluetooth permission (only  on iOS 13+). If false, you must invoke `Visit.shared.requestBluetoothPermission` or otherwise obtain Bluetooth permission before Visit will scan for Bluetooth beacons manually or automatically.

## `VSTNetworkService`

`VSTNetworkService` is a fully functional implementation of VSTDelegate that, if used as the delegate for Visit, will handle configuration and Location/Beacon batches by posting data to urls that you provide. You must provide events and configuration urls via the `setEventsURL(withString str : String)` and `setConfigurationURL(withString str: String)`, respectively.

### Examples
#### Default Implementation
For the most basic implementation of the SDK, use the following as a template for your `AppDelegate`:
```
private let VSTDelegate = VSTNetworkService()

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
  VSTDelegate.setEventsURL(withString: "http://my.product.com/api/events")
  VSTDelegate.setConfigurationURL(withString: "http://my.product.com/api/config")
  VSTDelegate.setHeader(key: "Content-Type", value: "application/json")

  Visit.shared.delegate = VSTDelegate
  Visit.shared.start(launchOptions: launchOptions, onUserLaunch: { options in
    /*
    ** Launch Logic
    */
  })
  return true
}
```

`VSTNetworkService` also allows you to set headers to be included in each call to your provided urls. You can manipulate these headers via the `setHeader(key: String, value: String)`, `removeHeader(key: String)`, and `clearHeaders()` members.

#### Default Implementation with Interceptors
If you want to utilize these standard implementations, but also want to intercept the processing of locations and beacons or configuration events of the Visit SDK,  You can implement your `AppDelegate` like this:

```
class AppDelegate: UIResponder, UIApplicationDelegate, VSTDelegate {
  var window: UIWindow?
  let VSTNetworkService = VSTNetworkService()

  func getConfiguration(success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
    // HERE: Do something you need to do before the SDK requests config info
    VSTNetworkService.getConfiguration(success: { config in
        // HERE: Do something you need to do with the results of the config call before invoking the default callback
        success(config)
      }, failure: { error in
        // HERE: Do something you need to do with the error before invoking the default callback
        failure(error)
      })
  }

  func process(_ location: CLLocation, with mode: VSTMovementMode) {
    // HERE: Do something you need to do with the location or mode before VSTNetworkService processes it
    VSTNetworkService.process(location, with: mode)
  }

  func process(_ beacons: Set<VSTBeacon>, at location: CLLocation) {
    // HERE: Do something you need to do with the beacons before VSTNetworkService processes it
    VSTNetworkService.process(beacons, at: location)
  }

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    VSTDelegate.setEventsURL(withString: "http://my.product.com/api/events")
    VSTDelegate.setConfigurationURL(withString: "http://my.product.com/api/config")
    VSTDelegate.setHeader(key: "Content-Type", value: "application/json")

    Visit.shared.delegate = self
    Visit.shared.start(launchOptions: launchOptions, onUserLaunch: { options in
      /*
      ** Launch Logic
      */
    })
    return true
  }
}
```
#### Fully Custom implementation
The Visit SDK does not require that you communicate with any API. If you want to handle everything offline, that is your prerogative and is fully supported by the Visit SDK.

```
class AppDelegate: UIResponder, UIApplicationDelegate, VSTDelegate {
  var window: UIWindow?

  func getConfiguration(success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
    // Do whatever you want here, just make sure to invoke success or failure when you're done.
    success([:])
  }

  func process(_ location: CLLocation, with mode: VSTMovementMode) {
    // HERE: Do whatever location processing your app wants to do.
  }

  func process(_ beacons: Set<VSTBeacon>, at location: CLLocation) {
    // HERE: Do whatever beacon processing your app wants to do.
  }

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    Visit.shared.delegate = self
    Visit.shared.start(launchOptions: launchOptions, onUserLaunch: { options in
      /*
      ** Launch Logic
      */
    })
    return true
  }
}
```

### Additional Ways to Control SDK Behavior

In addition to the `getConfig`

### `VSTMovementMode`

#### Overview

`VSTMovementMode` is an `enum` that represent the possible modes of movement in which a device may exist:

#### Values

`.unknown` represents a state in which cannot determine a movement mode, typically only seen whenever location scanning begins.

`.stationary` means the device is currently not moving.

`walking` means the user of the device is likely walking.

`running` means that the user of the device is likely running.

`.city` means the device is likely inside an automobile that is currently driving in a city.

`.highway` means that the device is likely inside an automobile that is currently driving on a highway.


## Requirements
Visit requires at least iOS 9.3 to run. Beacon scanning requires a Bluetooth LE capable device to operate.

# License
Visit is available under the MIT license. See the LICENSE file for more info.
