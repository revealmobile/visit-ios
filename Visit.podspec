Pod::Spec.new do |s|
  s.name             = 'Visit'
  s.version          = '1.0.0'
  s.summary          = 'Visit is an SDK that simplifies the process of Location and Beacon monitoring.'
  s.swift_version    = '5.2'
  s.description      = <<-DESC
The Visit SDK was created to solve the complex issues around scanning for beacons and monitoring user locations.  
It has been built to be as simple as possible to begin the process of collecting this data while allowing extensibility.
                       DESC

  s.homepage         = 'https://bitbucket.org/revealmobile/visit-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'seanbdoherty' => 'sean.doherty@crosscomm.com' }
  s.source           = { :git => 'https://bitbucket.org/revealmobile/visit-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'Visit/Classes/**/*'
  
  s.ios.vendored_frameworks = 'VSTCore.framework'
end
