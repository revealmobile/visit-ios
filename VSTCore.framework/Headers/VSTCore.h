//
//  VSTCore.h
//  VSTCore
//
//  Created by Ben Roaman on 8/15/19.
//  Copyright © 2019 reveal. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for VSTCore.
FOUNDATION_EXPORT double VSTCoreVersionNumber;

//! Project version string for VSTCore.
FOUNDATION_EXPORT const unsigned char VSTCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VSTCore/PublicHeader.h>


