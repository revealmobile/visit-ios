//
//  VSTUpdateStyle.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/15/19.
//

import Foundation

@objc public enum VSTUpdateStyle: Int {
    case relaxed = 0, regular, aggressive, undefined

    public var name: String {
        switch self {
        case .relaxed: return "Relaxed"
        case .regular: return "Regular"
        case .aggressive: return "Aggressive"
        case .undefined: return "Undefined"
        }
    }
}
