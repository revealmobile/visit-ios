//
//  Visit.swift
//

import Foundation
import UIKit
import CoreLocation
import VSTCore

public class Visit {
    // MARK: Singleton
    
    /// The Visit Singleton
    public static let shared = Visit()
    
    /// Initializer
    private init() {}
    
    // MARK: Instance Variables
    
    /// An implementation of VisitDelegate.
    private var delegate: VSTDelegate?
    
    // MARK: Instance Variables - Client Preferences
    
    /**
     If true, Visit will automatically request location permission.
    */
    public var canRequestLocationPermissionAutomatically = true
    /**
     Determines the level of Location Auth that Visit will attempt to obtain when automatically requesting Location Auth.
     */
    public var preferredLocationPermission: CLAuthorizationStatus = .authorizedAlways
    /**
     Determines whether visit is allowed to automatically scan for BLE Beacons
     */
    public var canScanForBLEBeaconsAutomatically = true
    /**
     Determines whether Visit is allowed to automatically request Bluetooth Permission (iOS 13+).
     */
    public var canRequestBLEPermissionAutomatically = true
    /**
     Determines whether Visit is allowed to automatically scan for IBeacons.
     */
    public var canScanForIBeaconsAutomatically = true
    /**
     Determines if, on a manual launch after a background launch, the SDK will automatically start all of its features.
     */
    public var startsOnTrueLaunch = true
//    /**
//     If false, provides a failsafe against the application launching in the background for significant location updates.
//
//     If your app does not have background location updates, or does not have 'always' location permission, or you have provided a value of 'false' for the 'shouldLaunchInBackground' key in your configuration this should never be necessary.
//     */
//    public var shouldLaunchInBackgroundOverride = true
    
    // MARK: Computed Properties
    /**
     Represents whether the SDK has started.
     
     This will be set to true after the start method is invoked and the first successfull invocation of delegate.getConfiguration, otherwise false.
     */
    public var started: Bool {
        return VSTCore.shared.started
    }
    
//    private var launchedToHandleLocationEvent = false
//    private var launchedToHandleBLEEvent = false
    
    
    
//    private var userLaunchCallback: (([UIApplication.LaunchOptionsKey: Any]?) -> Void)?
//    private var originalLaunchOptions: [UIApplication.LaunchOptionsKey: Any]?
    
    public var currentMovementMode: VSTMovementMode {
        return VSTMovementMode(rawValue: VSTCore.shared.currentMovementMode.rawValue) ?? .undefined
    }
    
    public var currentUpdateState: VSTManagerState {
        return VSTManagerState(rawValue: VSTCore.shared.currentUpdateState.rawValue) ?? .undefined
    }
    
    public var currentUpdateStyle: VSTUpdateStyle {
        return VSTUpdateStyle(rawValue: VSTCore.shared.currentUpdateStyle.rawValue) ?? .undefined
    }
    
    // MARK: Constants
    /// A source for all configuration keys that should be provided in the SDK configuration.
    public struct configKeys {
        internal static let success = "success"
        internal static let mapKey = "map_key"
        internal static let errors = "errors"
        internal static let backgroundLocationDistanceFilter = "bkg_location_filter"
        internal static let foregroundLocationDistanceFiler = "fre_location_filter"
        internal static let personas = "personas"
        internal static let cachTTL = "cache_ttl"
        internal static let eddystoneCompletionTimeout = "eddystone_completion_timeout"
        internal static let securecastManufacturerCodes = "securecast_manufacturer_codes"
        /// The iBeacon UUIDs that the SDK will monitor. [String].
        public static let beacons = "beacons"
        /// If false, bluetooth scanning will be disabled. Bool.
        public static let discoveryEnabled = "discovery_enabled"
        /// The time between bluetooth scans in seconds. Double.
        public static let scanInterval = "scan_interval"
        /// The duration of a bluetooth scan in seconds. Int.
        public static let scanLength = "scan_length"
        /// Determines how old a location can be and still be considered viable for a beacon. Double.
        public static let locationFixTimeout = "location_fix_timeout"
        /// The size of batches sent to delegate.readyToProcess. Int.
        public static let batchSize = "batch_size"
        /// The minimum time between processing batches. Double.
        public static let batchTimeout = "batch_timeout"
        /// Determines whether batches will be processed in the background. Bool.
        public static let batchBackgroundSend = "batch_background_send"
        /// Determines whether the SDK will launch your app in the background in order to handle Significant Location Updates. Bool.
        public static let shouldLaunchInBackground = "should_launch_in_background"
    }
    /**
     Starts the SDK.
     
     It is recommended to start the Visit SDK in the didFinishLaunching method of your AppDelegate, but not required. If you do not plan to utilize the background launch/significant location functionality of the SDK, you do not need to provide launchOptions or onUserLaunch.
     
     - Parameters:
         - launchOptions: These should be the launch options passed into your didFinishLaunching implementation. They allow visit to determine the context of your app launch.
         - onUserLaunch: This is a callback provided to Visit to execute when your app is manually launched by a user AFTER being launched in the background to handle a significant location update.
     */
    public func start(launchOptions: [UIApplication.LaunchOptionsKey: Any]?, onUserLaunch: (([UIApplication.LaunchOptionsKey: Any]?) -> Void)?) -> Void {
        guard delegate != nil else {
            print("VISIT :: WARNING :: CANNOT START IF DELEGATE == NIL")
            return
        }
        // Set Client Preferences
        VSTCore.shared.canRequestLocationPermissionAutomatically = canRequestLocationPermissionAutomatically
        VSTCore.shared.preferredLocationPermission = preferredLocationPermission
        VSTCore.shared.canScanForBLEBeaconsAutomatically = canScanForBLEBeaconsAutomatically
        VSTCore.shared.canRequestBLEPermissionAutomatically = canRequestBLEPermissionAutomatically
        VSTCore.shared.canScanForIBeaconsAutomatically = canScanForIBeaconsAutomatically
        
        VSTCore.shared.start(launchOptions: launchOptions, onUserLaunch: onUserLaunch)
    }
}

// MARK: Direct Controls
extension Visit {
    public func executeFullBeaconScan() {
        VSTCore.shared.executeManualScan()
    }
    
    public func requestBluetoothPermission() {
        VSTCore.shared.requestBluetoothPermission()
    }

//    public func executeBLEBeaconScan() {
//        /// TODO:
//    }
//    
//    public func executeIBeaconScan() {
//        /// TODO:
//    }
//    
//    public func pauseLocationUpdates() {
//        /// TODO:
//    }
//    
//    public func resumeLocationUpdates() {
//        /// TODO:
//    }
//    
//    public func updateLocationOnce() {
//        /// TODO:
//    }
//    
//    public func requestLocationPermission() {
//        /// TODO:
//    }
}

// MARK: Delegate Setter
extension Visit {
    public func setDelegate(_ clientDelegate: VSTDelegate) {
//        guard delegate == nil else {
//            print("VISIT :: WARNING :: DELEGATE CAN ONLY BE SET ONCE PER LAUNCH")
//            return
//        }
        
        delegate = clientDelegate
        
        VSTCore.shared.getConfigCallback = { success, failure in
            self.delegate?.getConfiguration(success: success, failure: failure)
        }
        
        VSTCore.shared.processLocationCallback = { location, coreMode in
            let mode = VSTMovementMode(rawValue: coreMode.rawValue) ?? .undefined
            self.delegate?.processLocation(location, with: mode)
        }
        
        VSTCore.shared.processBeaconsCallback = { coreBeacons, location in
            let beacons = Set<VSTBeacon>(coreBeacons.map({ VSTBeacon($0) }))
            self.delegate?.processBeacons?(beacons, at: location)
        }
        
        VSTCore.shared.movementModeDidChangeCallback = { coreMode in
            let mode = VSTMovementMode(rawValue: coreMode.rawValue) ?? .undefined
            self.delegate?.movementModeDidChange?(to: mode)
        }
        
        VSTCore.shared.locationUpdatesDidPauseCallback = { region in
            self.delegate?.locationUpdatesDidPause?(with: region)
        }
        
        VSTCore.shared.locationUpdatesDidResumeCallback = {
            self.delegate?.locationUpdatesDidResume?()
        }
        
        VSTCore.shared.locationUpdateStyleDidChangeCallback = { coreStyle in
            let style = VSTUpdateStyle(rawValue: coreStyle.rawValue) ?? .undefined
            self.delegate?.updateStyleDidChange?(to: style)
        }
        
        VSTCore.shared.updateStateDidChangeCallback = { coreState in
            let state = VSTManagerState(rawValue: coreState.rawValue) ?? .undefined
            self.delegate?.updateStateDidChange?(to: state)
        }
        
        VSTCore.shared.didSkipLocationCallback = { location in
            self.delegate?.didSkipLocation?(location)
        }
        
        VSTCore.shared.shouldCreateLogsCallback = {
            return self.delegate?.shouldCreateLogs?() ?? false
        }
        
        VSTCore.shared.didCreateLogCallback = { message, coreLevel in
            let level = VSTLogLevel(rawValue: coreLevel.rawValue) ?? .undefined
            self.delegate?.didCreateLog?(with: message, at: level)
        }
        
        VSTCore.shared.processTerminatedLocationsCallback = { coreLocations in
            let locations = coreLocations.map({ CLLocation($0) })
            self.delegate?.processTerminatedLocations?(locations)
        }
    }
}

// MARK: BLE State Accessors
extension Visit {
    internal func isBluetoothPoweredOn() -> Bool {
        return VSTCore.shared.isBluetoothPoweredOn()
    }
    
    internal func isBluetoothSupported() -> Bool {
        return VSTCore.shared.isBluetoothSupported()
    }
    
    internal func isIBeaconsScanningAllowed() -> Bool {
        return VSTCore.shared.isIBeaconsScanningAllowed()
    }
}
