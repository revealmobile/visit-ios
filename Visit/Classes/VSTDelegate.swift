//
//  VSTDelegate.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/15/19.
//

import Foundation
import CoreLocation

/**
 Your implementation of the VisitDelegate protocol will allow the SDK to obtain configurations and process discovered beacons and locations.
 */
@objc public protocol VSTDelegate {
    /**
     Provides a configuration dictionary to the Visit SDK.
     
     This is designed to be functional in the case that you are obtaining a configuration asynchronously. Be sure to invoke the success method in order to actually pass the configuration into the SDK.
     
     - Parameters:
     - success: the method you invoke to actually pass your configuration into the SDK.
     - failure: the method you invoke if your fail to obtain/generate and configuration dictionary.
     */
    func getConfiguration(success:@escaping([String:Any]) -> Void, failure:@escaping(Error?) -> Void)
    
    func processLocation(_ location: CLLocation, with mode: VSTMovementMode)
    
    @objc optional func processBeacons(_ beacons: Set<VSTBeacon>, at location: CLLocation?)
    
    @objc optional func movementModeDidChange(to mode: VSTMovementMode)
    
    @objc optional func locationUpdatesDidPause(with region: CLCircularRegion)
    
    @objc optional func locationUpdatesDidResume()
    
    @objc optional func updateStyleDidChange(to style: VSTUpdateStyle)
    
    @objc optional func updateStateDidChange(to state: VSTManagerState)
    
    @objc optional func didSkipLocation(_ location: CLLocation)
    
    @objc optional func shouldCreateLogs() -> Bool
    
    @objc optional func didCreateLog(with message: String, at level: VSTLogLevel)
    
    @objc optional func processTerminatedLocations(_ locations: [CLLocation])
}
