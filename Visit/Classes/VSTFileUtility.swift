//
//  VSTFileUtility.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/16/19.
//

import UIKit
import CoreLocation
import VSTCore

public class VSTFileUtility {
    // MARK: Private Constants
    private static let launchLocationDirectory = "VSTLaunchLocations"
//    private static let suspendedPipelineFilename = "VSTSuspendedPipeline.json"
    private static let visitNetworkServiceRetryQueueDirectory = "VSTNetworkServiceRetryQueue"
    private static let suspendedVNSBatchLocationsFilename = "VSTSuspendedBatchLocations.json"
    private static let suspendedVNSBatchBeaconsFilename = "VSTSuspendedBatchBeacons.json"
    
    // MARK: Internal Functions - Launch Locations (Terminated -> Background)
    
    public static func readLaunchLocations() -> [VSTCoreLocation] {
        guard let launchLocationDirectory = getLaunchLocationDirectoryURL() else {
            return []
        }
        
        var locationEvents = [VSTCoreLocation]()
        
        do {
            let fileNames = try FileManager.default.contentsOfDirectory(atPath: launchLocationDirectory.path)
            for file in fileNames {
                guard let events: [VSTCoreLocation] = read(from: launchLocationDirectory.appendingPathComponent(file), as: [VSTCoreLocation].self) else {
                    VSTCore.log("VFU", "Unable to read file from Launch Locations", "\(file)", level: .error)
                    continue
                }
                locationEvents.append(contentsOf: events)
            }
        } catch {
            VSTCore.log("VFU", "Failed to read Launch Locations", "\(error.localizedDescription)", level: .error)
        }
        VSTCore.log("VFU", "Restoring \(locationEvents.count) Launch Locations")
        removeItem(at: launchLocationDirectory)
        return locationEvents
    }
    
    public static func clearLaunchLocationRecords() {
        guard let launchLocationDirectory = getLaunchLocationDirectoryURL() else {
            return
        }
        VSTCore.log("VFU", "Clearing stored Launch Locations.")
        removeItem(at: launchLocationDirectory)
    }
    
    // MARK: Internal Functions - VisitNetworkService Retry Queue
    internal static func writeBatchToVNSRetryQueue(_ batch: Data, as fileName: String? = nil) {
        guard let vnsRetryQueueDirectory = getVNSRetryQueueDirectoryURL() else {
            VSTCore.log("VFU", "Write failed batch failed", "Failed to generate retry queue directory URL", level: .error)
            return
        }
        let fileURL = vnsRetryQueueDirectory.appendingPathComponent(fileName != nil ? fileName! : "\(Int(Date().timeIntervalSince1970*100000)).json")
        VSTCore.log("VFU", "Writing batch to Retry Queue")
        write(batch, to: fileURL)
    }
    
    internal static func readNextBatchFromVNSRetryQueue() -> (Data?, String)? {
        guard let vnsRetryDirectory = getVNSRetryQueueDirectoryURL() else {
            return nil
        }
        
        do {
            guard let fileName = try FileManager.default.contentsOfDirectory(atPath: vnsRetryDirectory.path).sorted(by: { $0 < $1 }).first else {
                VSTCore.log("VFU", "No batches in Retry Queue")
                return nil
            }
            let fileURL = vnsRetryDirectory.appendingPathComponent(fileName)
            VSTCore.log("VFU", "Reading next batch from Retry Queue")
            let batch = (read(from: fileURL), fileName)
            removeItem(at: fileURL)
            return batch
        } catch {
            VSTCore.log("VFU", "Failed to read next batch in Retry Queue", "\(error)", level: .error)
            return nil
        }
    }
    
    public static func clearVNSRetryQueue() {
        guard let vnsRetryQueueDirectory = getVNSRetryQueueDirectoryURL() else {
            return
        }
        VSTCore.log("VFU", "Clearing VNS Retry Queue")
        removeItem(at: vnsRetryQueueDirectory)
    }
    
    // MARK: Internal Functions - VisitNetworkService Batch Storage
    public static func suspendVNSBatch(locations: [VSTCoreLocation], beacons: [VSTBeacon]) {
        VSTCore.log("VFU", "Suspending VNS batch")
        guard let locationsFileURL = getSuspendedVNSBatchLocationsFileURL(), let beaconsFileURL = getSuspendedVNSBatchBeaconsFileURL() else {
            return
        }
        write(locations, to: locationsFileURL)
        write(beacons, to: beaconsFileURL)
    }
    
    public static func reviveVNSBatch() -> ([VSTCoreLocation], [VSTBeacon]) {
        VSTCore.log("VFU", "Reviving suspended VNS batch")
        guard let locationsFileURL = getSuspendedVNSBatchLocationsFileURL(), let beaconsFileURL = getSuspendedVNSBatchBeaconsFileURL() else {
            return ([], [])
        }
        return (read(from: locationsFileURL, as: [VSTCoreLocation].self) ?? [], read(from: beaconsFileURL, as: [VSTBeacon].self) ?? [])
    }
    
    public static func clearVNSBatch() {
        VSTCore.log("VFU", "Clearing suspended VNS batch")
        guard let locationsFileURL = getSuspendedVNSBatchLocationsFileURL(), let beaconsFileURL = getSuspendedVNSBatchBeaconsFileURL() else {
            return
        }
        removeItem(at: locationsFileURL)
        removeItem(at: beaconsFileURL)
    }
    
    // MARK: Private Functions
    private static func getLaunchLocationDirectoryURL() -> URL? {
        return getCacheSubdirectoryURL(directory: launchLocationDirectory)
    }
    
    private static func getVNSRetryQueueDirectoryURL() -> URL? {
        return getCacheSubdirectoryURL(directory: visitNetworkServiceRetryQueueDirectory)
    }

    private static func getCacheURL() -> URL? {
        return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
    }
    
    private static func getCacheSubdirectoryURL(directory name: String) -> URL? {
        guard let url = getCacheURL()?.appendingPathComponent(name, isDirectory: true) else {
            VSTCore.log("VFU", "Unable to generate URL for cache subdirectory named \(name).", level: .error)
            return nil
        }
        
        if !FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
            } catch {
                VSTCore.log("VFU :: Unable to create cache subdirectory named \(name)", "\(error)", level: .error)
                return nil
            }
        }
        return url
    }
    
    private static func getSuspendedVNSBatchLocationsFileURL() -> URL? {
        return getFileURL(filename: suspendedVNSBatchLocationsFilename)
    }
    
    private static func getSuspendedVNSBatchBeaconsFileURL() -> URL? {
        return getFileURL(filename: suspendedVNSBatchBeaconsFilename)
    }
    
//    private static func getSuspendedPipelineFileURL() -> URL? {
//        return getFileURL(filename: suspendedPipelineFilename)
//    }
    
    private static func getFileURL(filename: String) -> URL? {
        guard let url = getCacheURL()?.appendingPathComponent(filename, isDirectory: false) else {
            VSTCore.log(" VFU", "Unable to generate file URL for file: \(filename).", level: .error)
            return nil
        }
        return url
    }
    
    private static func write<T: Encodable>(_ object: T, to url: URL) {
        do {
            let data = try JSONEncoder().encode(object)
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        } catch {
            VSTCore.log("VFU", "Failed to encode object", "\(error.localizedDescription)", level: .error)
        }
    }
    
//    private static func write(_ data: Data, to url: URL) {
//        FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
//    }
    
    private static func read<T: Decodable>(from fileUrl: URL, as type: T.Type) -> T? {
        if let data = FileManager.default.contents(atPath: fileUrl.path) {
            do {
                return try JSONDecoder().decode(type, from: data)
            } catch {
                VSTCore.log("VFU", "Failed to decode data from file", "Path: \(fileUrl.path)", "\(error.localizedDescription)", level: .error)
                return nil
            }
        }
        VSTCore.log("VFU", "Failed to retreive data from file", "Path: \(fileUrl.path)", "File may not exist.", level: .error)
        return nil
    }
    
    private static func read(from fileURL: URL) -> Data? {
        return FileManager.default.contents(atPath: fileURL.path)
    }
    
    private static func clearContents(of directory: URL) {
        guard let contentsEnumerator = FileManager.default.enumerator(at: directory, includingPropertiesForKeys: nil) else {
            return
        }
        while let file = contentsEnumerator.nextObject() as? String {
            let fileURL = directory.appendingPathComponent(file)
            removeItem(at: fileURL)
//            do {
//                try FileManager.default.removeItem(at: fileURL)
//            } catch {
//                Visit.log("VFU :: Failed to Remove file with path \(fileURL.path) with error :: \(error)")
//                continue
//            }
        }
    }
    
    private static func removeItem(at url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            VSTCore.log("VFU", "Failed to remove file or directory", "Path: \(url.path)", "\(error.localizedDescription)", level: .error)
        }
    }
    
    // MARK: Debug Functions - Use to analyze a directory or json returned from a file
//    private static func logFileStructure() {
//        let cachesUrl = ()
//        do {
//            let objectDirectories = try FileManager.default.contentsOfDirectory(atPath: cachesUrl.path)
//            for directory in objectDirectories {
//                let directoryURL = cachesUrl.appendingPathComponent(directory)
//                let directoryContents = try FileManager.default.contentsOfDirectory(atPath: directoryURL.path)
//                NSLog("CONTENTS OF DIRECTORY \(directory) :: \(directoryContents)")
//            }
//        } catch {
//            NSLog("ERROR LOGGING CONTENTS OF DOCUMENT DIRECTORY :: \(error)")
//        }
//    }
//
//    private static func analyzeJSON<T: Codable>(object: Data, type: T.Type) {
//        do {
//            let dict = try JSONSerialization.jsonObject(with: object, options: .allowFragments) as! T
//            let formattedData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
//            let jsonString = String(data: object, encoding: .utf8)
//            print(jsonString)
//            print("conversion success")
//        } catch {
//            print("conversion failure")
//        }
//    }
}
