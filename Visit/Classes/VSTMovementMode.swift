//
//  File.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/15/19.
//

import Foundation

@objc public enum VSTMovementMode: Int {
    case unknown = 0, city, highway, walking, running, stationary, undefined
    
    public var name: String {
        switch self {
        case .unknown: return "Unknown"
        case .city, .highway: return "Driving"
        case .walking, .running: return "On Foot"
        case .stationary: return "Stationary"
        case .undefined: return "Undefined"
        }
    }
}
