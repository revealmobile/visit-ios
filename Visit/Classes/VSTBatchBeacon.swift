//
//  VSTBatchBeacon.swift
//  Visit
//
//  Created by Ben Roaman on 11/16/18.
//

import Foundation
import CoreLocation
import VSTCore

/**
 * Representation of beacon modeled for network calls
 **/
internal struct VSTBatchBeacon: Encodable {
    public let location: VSTCoreLocation
    public let beaconUUID: String
    public let beaconMajor: String
    public let beaconMinor: String
    public var beaconMac: String?
    public let beaconRSSI: Int
    public let beaconProximity: String
    public let beaconAccuracy: Double
    public let beaconType: String
    public let beaconVendor: Int
    public let beaconVendorKey: Int
    public let beaconVendorName: String
    public var beaconPayload: Data?
    public var beaconURL: String?
    public let dwellTime: Int
    public let discoveryTime: Double
    public let lastSeenTime: Double
    
    private enum CodingKeys: String, CodingKey {
        case location
        case beaconUUID = "beacon_uuid"
        case beaconMajor = "beacon_major"
        case beaconMinor = "beacon_minor"
        case beaconMac = "beacon_mac"
        case beaconRSSI = "beacon_rssi"
        case beaconProximity = "beacon_proximity"
        case beaconAccuracy = "beacon_accuracy"
        case beaconType = "beacon_type"
        case beaconVendor = "beacon_vendor"
        case beaconVendorKey = "beacon_vendor_key"
        case beaconVendorName = "beacon_vendor_name"
        case beaconPayload = "beacon_payload"
        case beaconURL = "beacon_url"
        case discoveryTime
        case lastSeenTime
        case dwellTime
    }
    
    private static func getProximityString(from proximity: CLProximity) -> String {
        switch (proximity) {
        case .unknown: return "unknown"
        case .near: return "near"
        case .immediate: return "immediate"
        case .far: return "far"
        @unknown default: return "unknown"
        }
    }
    
    public init(_ beacon: VSTBeacon) {
        location = VSTCoreLocation(beacon.location ?? CLLocation())
        beaconUUID = beacon.proximityUUID
        beaconMajor = beacon.major
        beaconMinor = beacon.minor
        beaconMac = beacon.macAddress
        beaconRSSI = beacon.rssi
        beaconProximity = VSTBatchBeacon.getProximityString(from: beacon.proximity)
        beaconAccuracy = beacon.accuracy
        beaconType = beacon.vendorName ?? "unknown"
        beaconVendor = beacon.vendorCode ?? -1
        beaconVendorKey = beacon.vendorCode ?? -1
        beaconVendorName = beacon.vendorName ?? "unknown"
        beaconPayload = beacon.payload
        beaconURL = beacon.url
        dwellTime = Int(beacon.updatedAt.timeIntervalSince(beacon.createdAt))
        discoveryTime = beacon.createdAt.timeIntervalSince1970
        lastSeenTime = beacon.updatedAt.timeIntervalSince1970
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(location, forKey: .location)
        try container.encode(beaconUUID, forKey: .beaconUUID)
        if !beaconMajor.isEmpty { try container.encode(beaconMajor, forKey: .beaconMajor) }
        if !beaconMinor.isEmpty { try container.encode(beaconMinor, forKey: .beaconMinor) }
        if let mac = beaconMac { try container.encode(mac, forKey: .beaconMac) }
        try container.encode(beaconRSSI, forKey: .beaconRSSI)
        try container.encode(beaconProximity, forKey: .beaconProximity)
        try container.encode(beaconAccuracy, forKey: .beaconAccuracy)
        try container.encode(beaconType, forKey: .beaconType)
        try container.encode(beaconVendor, forKey: .beaconVendor)
        try container.encode(beaconVendorKey, forKey: .beaconVendorKey)
        try container.encode(beaconVendorName, forKey: .beaconVendorName)
        if let payload = beaconPayload { try container.encode(payload, forKey: .beaconPayload) }
        if let url = beaconURL { try container.encode(url, forKey: .beaconURL) }
        try container.encode(dwellTime, forKey: .dwellTime)
        try container.encode(discoveryTime, forKey: .discoveryTime)
        try container.encode(lastSeenTime, forKey: .lastSeenTime)
    }
}
