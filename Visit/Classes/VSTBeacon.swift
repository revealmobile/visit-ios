//
//  VSTBeacon.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/15/19.
//

import Foundation
import CoreLocation
import VSTCore

/**
 * Representation of a Beacon
 */
public class VSTBeacon: NSObject, Codable {
    
    override public var hash: Int {
        var hasher = Hasher()
        hasher.combine(proximityUUID)
        hasher.combine(major)
        hasher.combine(minor)
        return hasher.finalize()
    }
    
    override public func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? VSTBeacon else {
            return false
        }
        
        if (!self.major.isEmpty && !other.major.isEmpty && !self.minor.isEmpty && !other.minor.isEmpty) {
            return self.proximityUUID == other.proximityUUID && self.major == other.major && self.minor == other.minor
        } else {
            return self.proximityUUID == other.proximityUUID
        }
    }
    
    public private(set) var createdAt: Date
    public var updatedAt: Date
    
    /**
     Proximity identifier associated with the beacon.
     */
    public private(set) var proximityUUID: String
    
    /**
     Most significant value associated with the beacon.
     */
    public private(set) var major: String
    
    /**
     Least significant value associated with the beacon.
     */
    public private(set) var minor: String
    
    /**
     Proximity of the beacon from the device.
     */
    public private(set) var proximity: CLProximity
    
    /**
     Horizontal accuracy of the beacon to the device.
     */
    public private(set) var accuracy: CLLocationAccuracy
    
    /**
     Received signal strength associated with the beacon.
     */
    public private(set) var rssi: Int
    
    /**
     Location of scan that discovered this beacon
     */
    public private(set) var location: CLLocation?
    
    /**
     Optional vendor name associated with the beacon.
     */
    public private(set) var vendorName: String?
    
    /**
     Optional vendor numeric code associated with the beacon.
     */
    public private(set) var vendorCode: Int?
    
    /**
     Optional mac address associated with the beacon.
     */
    public private(set) var macAddress: String?

    /**
     Optional url associated with the beacon (Mainly for eddystone beacons).
     */
    public private(set) var url: String?
    
    /**
     The entire data packet associated with the beacon (Useful for packets needing to be encrypted via 3rd party like Gimbal beacons).
     */
    public private(set) var payload: Data?
    
//    /**
//     The last time the beacon event was put in the .readyToProcess pipeline.  You can consider this the time the SDK finished with the event.
//    */
//    public let lastSent: Date?
    
    // Provide override for the vendor info from unknown beacons.
//    private var _vendorName: String?
//    private var _vendorCode: Int?

    private enum CodingKeys: String, CodingKey {
        case createdAt
        case updatedAt
        case proximityUUID = "beacon_uuid"
        case major = "beacon_major"
        case minor = "beacon_minor"
        case proximity = "beacon_proximity"
        case accuracy = "beacon_accuracy"
        case rssi = "beacon_rssi"
        case location
        case vendor = "beacon_type"
        case beaconCode = "beacon_vendor"
        case macAddress = "beacon_mac"
        case url = "beacon_url"
        case payload
    }

//    internal init(proximityUUID: String,
//                major: String = "",
//                minor: String = "",
//                proximity: CLProximity = .unknown,
//                accuracy: CLLocationAccuracy = 0,
//                rssi: Int = 0) {
//        self.proximityUUID = proximityUUID
//        self.major = major
//        self.minor = minor
//        self.proximity = proximity
//        self.accuracy = accuracy
//        self.rssi = rssi
//        createdAt = Date()
//        updatedAt = Date()
//    }
//
//    internal convenience init(beacon: CLBeacon) {
//        vendorName = "iBeacon"
//        self.init(proximityUUID: beacon.proximityUUID.uuidString,
//                   major: beacon.major.stringValue,
//                   minor: beacon.minor.stringValue,
//                   proximity: beacon.proximity,
//                   accuracy: beacon.accuracy,
//                   rssi: beacon.rssi
//        )
//    }
    
    internal init(_ coreBeacon: VSTCoreBeacon) {
//        self.init()
        createdAt = coreBeacon.createdAt
        updatedAt = coreBeacon.updatedAt
        proximityUUID = coreBeacon.proximityUUID
        major = coreBeacon.major
        minor = coreBeacon.minor
        proximity = coreBeacon.proximity
        accuracy = coreBeacon.accuracy
        rssi = coreBeacon.rssi
        location = coreBeacon.location
        vendorName = coreBeacon.vendorName
        vendorCode = coreBeacon.vendorCode
        macAddress = coreBeacon.macAddress
        url = coreBeacon.url
        payload = coreBeacon.payload
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        proximityUUID = try container.decode(String.self, forKey: .proximityUUID)
        major = try container.decode(String.self, forKey: .major)
        minor = try container.decode(String.self, forKey: .minor)
        if let rawProximity = try container.decodeIfPresent(Int.self, forKey: .proximity) {
            proximity = CLProximity(rawValue: rawProximity) ?? .unknown
        } else {
            proximity = .unknown
        }
        accuracy = try container.decode(CLLocationAccuracy.self, forKey: .accuracy)
        rssi = try container.decode(Int.self, forKey: .rssi)
        if let locationModel = try container.decodeIfPresent(VSTCoreLocation.self, forKey: .location) {
            location = CLLocation(locationModel)
        }
        vendorName = try container.decodeIfPresent(String.self, forKey: .vendor)
        vendorCode = try container.decodeIfPresent(Int.self, forKey: .beaconCode)
        macAddress = try container.decodeIfPresent(String.self, forKey: .macAddress)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        payload = try container.decodeIfPresent(Data.self, forKey: .payload)
//        if let lastSentEpoch = try container.decodeIfPresent(Int.self, forKey: .lastSent) {
//            lastSent = Date.from(epoch: lastSentEpoch)
//        }
        createdAt = try container.decode(Date.self, forKey: .createdAt)
        updatedAt = try container.decode(Date.self, forKey: .updatedAt)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(proximityUUID, forKey: .proximityUUID)
        try container.encode(major, forKey: .major)
        try container.encode(minor, forKey: .minor)
        try container.encode(proximity.rawValue, forKey: .proximity)
        try container.encode(accuracy, forKey: .accuracy)
        try container.encode(rssi, forKey: .rssi)
        try container.encode(vendorCode, forKey: .beaconCode)
        if location != nil {
            let codableLocation = VSTCoreLocation(location!)
            try container.encode(codableLocation, forKey: .location)
        }
        try container.encode(vendorName, forKey: .vendor)
        try container.encode(macAddress, forKey: .macAddress)
        try container.encode(url, forKey: .url)
        try container.encode(payload, forKey: .payload)
//        try container.encode(lastSent?.epochMilis(), forKey: .lastSent)
        try container.encode(createdAt, forKey: .createdAt)
        try container.encode(updatedAt, forKey: .updatedAt)
    }
    
    public static func ==(lhs: VSTBeacon, rhs: VSTBeacon) -> Bool {
        // If both sides have major and minor, compare all three values
        if (!lhs.major.isEmpty && !rhs.major.isEmpty && !lhs.minor.isEmpty && !rhs.minor.isEmpty) {
            return lhs.proximityUUID == rhs.proximityUUID && lhs.major == rhs.major && lhs.minor == rhs.minor
        } else {
            return lhs.proximityUUID == rhs.proximityUUID
        }
    }
}
