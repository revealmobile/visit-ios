//
//  VSTManagerState.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/15/19.
//

import Foundation

@objc public enum VSTManagerState: Int {
    case off = 0, on, paused, terminated, undefined
    
    public var name: String {
        switch self {
        case .off: return "Off"
        case .on: return "On"
        case .paused: return "Paused"
        case .terminated: return "Terminated"
        case .undefined: return "Undefined"
        }
    }
}
