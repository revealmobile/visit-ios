//
//  VSTBatchLocation.swift
//  Visit
//
//  Created by Ben Roaman on 11/16/18.
//

import Foundation
import VSTCore

/**
 * Representation of location modeled for network calls
 **/
internal class VSTBatchLocation: Encodable {
    public let lastSeenTime: Date
    public let discoveryTime: Date
    public let location: VSTCoreLocation
    
    public init(_ location: VSTCoreLocation) {
        lastSeenTime = location.timestamp
        discoveryTime = location.timestamp
        self.location = location
    }
    
    private enum CodingKeys: String, CodingKey {
        case discoveryTime
        case lastSeenTime
        case location
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(discoveryTime.timeIntervalSince1970, forKey: .discoveryTime)
        try container.encode(lastSeenTime.timeIntervalSince1970, forKey: .lastSeenTime)
        try container.encode(location, forKey: .location)
    }
}
