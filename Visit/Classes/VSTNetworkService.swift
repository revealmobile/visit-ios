//
//  VSTNetworkService.swift
//  Visit
//
//  Created by Ben Roaman on 1/4/18.
//

import Foundation
import AdSupport
import CoreLocation
import CoreTelephony
import SystemConfiguration
import VSTCore

internal enum HTTPMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
    case head = "HEAD"
    case connect = "CONNECT"
    case options = "OPTIONS"
    case patch = "PATCH"
    case trace = "TRACE"
}

/**
 A ready-made VisitDelegate that, provided urls and appropriate headers, handles retrieving configurations and processing events via server. On event network failures, VisitNetworkService will automatically save and later retry your events.
 */
open class VSTNetworkService: NSObject {
    
    internal struct batchResponseKeys {
        public static let success = "success"
        public static let errors = "errors"
        public static let personas = "personas"
    }
    
    // MARK: Instance Variables
    
    // TESTING CODE
//    fileprivate var sentBeacons = Set<String>()
//    fileprivate var duplicateBeacons = [String: Int]()
    
    /**
     The url used to post your events for processing.
    */
    public private(set) var eventsURL: URL?
    /**
     The url used to get your configurations.
    */
    public private(set) var configurationURL: URL?
    /**
     Headers used on event AND configurations calls.
    */
    public private(set) var headers: [String: String] = [:]
    /**
     The most recent responsed recieved from the server.
    */
    
    private var currentBatchBeacons: [VSTBeacon] = []
    private var currentBatchLocations: [VSTCoreLocation] = []
    public var currentBatchSize: Int {
        return currentBatchLocations.count + currentBatchBeacons.count
    }

    public private(set) var batchSize = 100
    public private(set) var batchTimeoutThreshold = 1200.0 // default value 20 min
    public private(set) var lastBatchSent = Date()
    public private(set) var shouldSendInBackground = true
    public private(set) var lastResponse: [String: Any] = [:]
    
    fileprivate var backgroundTaskIdentifier: UIBackgroundTaskIdentifier = .invalid
    fileprivate var sessionConfig: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = true
        config.timeoutIntervalForRequest = 1
        config.timeoutIntervalForResource = 10
        return config
    }()
    
    /**
     Initializer that allows your to provide and API key. Automatically inserts it into your headers under the key X-API-KEY.
     
     - Parameter apiKey: The API key for your server. Inserted into headers under key X-API-KEY.
    */
    public convenience init(apiKey: String) {
        self.init()
        headers["X-API-KEY"] = apiKey
    }
    
    override public init() {
        super.init()
        reviveBatch()
    }
    
    /**
     Sets the eventsURL.
     
     - Parameter withString: A string representation of your URL.
    */
    public func setEventsURL(withString str : String) { // TODO: Change this to batch url
        VSTCore.log("VNS", "Setting events URL", str)
        eventsURL = URL(string: str)
    }
    
    /**
     Sets the configurationURL.
     
     - Parameter withString: A string representation of your URL
    */
    public func setConfigurationURL(withString str: String) {
        VSTCore.log("VNS", "Setting configuration URL", str)
        configurationURL = URL(string: str)
    }
    
    /**
     Adds a header to all of your network calls.
     
     - Parameters:
         - key: The key of the header.
         - value: the value of the header.
    */
    public func setHeader(key: String, value: String) {
        VSTCore.log("VNS", "Setting Header", "key: \(key)", "value: \(value)")
        headers[key] = value
    }
    
    /**
     Removes the header with the provided key from your network calls.
     
     - Parameter key: The key of the header to be removed from your netork calls.
    */
    public func removeHeader(key: String) {
        VSTCore.log("VNS", "Removing Header", key)
        headers.removeValue(forKey: key)
    }

    /**
     Removes all headers from your network calls.
    */
    public func clearHeaders() {
        VSTCore.log("VNS", "Clearing Headers")
        headers = [:]
    }

    /**
     Sets the session configuration.
     
     This is how you customize a lot of the network functionality of the VisitNetworkService (Anything provided for by URLSessionConfiguration).
     
     - Parameter with: The session configuration to use on your network calls.
    */
    public func setSessionConfiguration(with config: URLSessionConfiguration) {
        VSTCore.log("VNS", "Setting Session Configuration")
        sessionConfig = config
    }
    
    // Provides boilerplate network call for clients to send events to their own servers, and handle whatever models they expect back
    private func sendBatch(url: URL?, events: VSTBatchParams, headers: [String: String]? = nil, success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
        do {
            let body = try JSONEncoder().encode(events)
//            analyzeJSON(object: body)
            call(url: url, method: .post, body: body, headers: headers, success: { result in
                // On success, we check for the next batch queued for retry
                if let queuedBatch = VSTFileUtility.readNextBatchFromVNSRetryQueue(), let batch = queuedBatch.0 {
                    self.call(url: url, method: .post, body: batch, headers: headers, success: { result in
                        VSTCore.log("VNS", "Successfully reposted failed batch")
                    }, failure: { error in
                        // If the retry fails, write the data back to the queue with the original filename
                        VSTCore.log("VNS", "Failed to repost failed batch", error?.localizedDescription ?? "no_error", level: .error)
                        VSTFileUtility.writeBatchToVNSRetryQueue(body, as: queuedBatch.1)
                    })
                }
                success(result)
            }, failure: { error in
                VSTFileUtility.writeBatchToVNSRetryQueue(body)
                failure(error)
            })
        } catch {
            VSTCore.log("VNS", "Send batch failed", "Failed to encode events", (error.localizedDescription), level: .error)
            handleError(error: error, callback: failure)
        }
    }
    
    // Provides boilerplate method for retrieving configuration from a custom server
    private func requestConfiguration(url: URL?, headers: [String : String]?, body: VSTConfigParams?, success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
        do {
            var argument: Data?
            if let _ = body {
                argument = try JSONEncoder().encode(body)
            }
            call(url: url, method: .post, body: argument, headers: headers, success: success, failure: failure)
        } catch {
            VSTCore.log("VNS", "Failed to encode configure request", error.localizedDescription, level: .error)
            handleError(error: error, callback: failure)
        }
    }
    
    // Base Generic Call for all networking
    private func call(url: URL?, method: HTTPMethod, body: Data?, headers: [String: String]?, success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
        VSTCore.log("VNS", "Invoking Network Call", "URL: \(url?.absoluteString ?? "nil url")", "Method: \(method.rawValue)", "Body: \(String(data:body!, encoding: .utf8) ?? "no body")", "Headers: \(headers ?? [:])")
        // Make sure url exists
        guard let url = url else {
            VSTCore.log("VNS", "Aborting network call", "Invalid URL", level: .error)
            return handleError(error: nil, callback: failure)
        }
        // Create request
        var request = URLRequest(url: url)
        // Set Method
        request.httpMethod = method.rawValue
        // Set body if one is provided
        if let body = body {
            request.httpBody = body
        }
        // Set headers if they are provided
        if let headers = headers {
            for key in headers.keys {
                // make sure there is a value for the key
                if let value = headers[key] {
                    if let _ = request.value(forHTTPHeaderField: key) {
                        request.setValue(value, forHTTPHeaderField: key)
                    } else {
                        request.addValue(value, forHTTPHeaderField: key)
                    }
                } else {
                    // If there is no value, log a notification but continue with call
                    VSTCore.log("VNS", "Header '\(key)' skipped", "No value for key")
                }
            }
        }

        DispatchQueue.global(qos: .background).async {
            // Create task for sending to server as url session to ensure delivery
            let task = URLSession(configuration: self.sessionConfig).dataTask(with: request) { d, r, e in
                if let response = r as? HTTPURLResponse, let data = d, e == nil {
                    // Check response code
                    guard response.statusCode == 200 else {
                        VSTCore.log("VNS", "Call failed with code \(response.statusCode)", level: .error)
                        return self.handleError(error: nil, callback: failure)
                    }
                    VSTCore.log("VNS", "Call succeeded")
                    // Attempt to deserialize JSON response
                    guard data.count > 0 else {
                        VSTCore.log("VNS", "Empty response")
                        return self.handleSuccess(result: [:], callback: success)
                    }
                    do {
                        guard let deserializedDictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                            VSTCore.log("VNS", "Cannot interpret deserialized return value", level: .error)
                            return self.handleError(error: nil, callback: failure)
                        }
                        return self.handleSuccess(result: deserializedDictionary, callback: success)
                    } catch {
                        VSTCore.log("VNS", "Cannot deserialize return value", error.localizedDescription, level: .error)
                        self.handleError(error: error, callback: failure)
                    }
                } else {
                    VSTCore.log("VNS", "Call failed", "\(e?.localizedDescription ?? "NO ERROR OBJECT")", level: .error)
                    self.handleError(error: e, callback: failure)
                }
            }
            // Execute task
            self.backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(withName: "VisitNetworkServiceSending") {
                task.cancel()
                UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
                self.backgroundTaskIdentifier = .invalid
            }
            task.resume()
        }
    }
    
    private func handleError(error: Error?, callback: (Error?) -> Void) {
        callback(error)
        UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
        self.backgroundTaskIdentifier = .invalid
    }
    
    private func handleSuccess(result: [String:Any], callback: ([String:Any]) -> Void) {
        self.lastResponse = result
        UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
        self.backgroundTaskIdentifier = .invalid
        return callback(result)
    }
    
    private func getParams(_ asBatchParams: Bool = false) -> VSTBaseParams {
        let params = asBatchParams ? VSTBatchParams() : VSTConfigParams()
        params.os = "ios"
        params.deviceId = UIDevice.current.identifierForVendor?.uuidString ?? "no_value"
        params.appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "no_value"
        params.sdkVersion = "0.1" /// TODO:
        params.appId = Bundle.main.bundleIdentifier ?? "no_value"
        params.timeZone = NSTimeZone.default.abbreviation() ?? "no_value"
        params.locationSharingEnabled = CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        params.locationPermissionRequested = "Always" /// TODO:
        params.version = UIDevice.current.systemVersion
        params.locale = NSLocale.current.identifier
        params.conType = "wifi" // TODO:
        params.idfa = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        params.supportsBLE = Visit.shared.isBluetoothSupported()
        params.manufacturer = "Apple"
        params.bluetoothEnabled = Visit.shared.isBluetoothPoweredOn()
        params.make = UIDevice.current.model
        params.model = UIDevice.modelName
        params.carrier = CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName
        return params
    }
    
    internal func currentBatchDidUpdate() {
        // if the current batch is the correct size, or if there is at least one event and the time threshold has been met, process the current batch
        if currentBatchSize >= batchSize || (currentBatchSize > 0 && Date().timeIntervalSince(lastBatchSent) > batchTimeoutThreshold) {
            processCurrentBatch()
        }
    }
    
    internal func processCurrentBatch() {
        let beacons = currentBatchBeacons
        let locations = currentBatchLocations
        currentBatchBeacons = []
        currentBatchLocations = []
        processBatch(locations: locations, beacons: beacons)
        lastBatchSent = Date()
    }
    
    internal func processBatch(locations: [VSTCoreLocation], beacons: [VSTBeacon]) {
        let batch = getParams(true) as! VSTBatchParams
        batch.locations = locations.map({ VSTBatchLocation($0) })
        batch.beacons = beacons.map({ VSTBatchBeacon($0) })
        
        sendBatch(url: eventsURL, events: batch, headers: headers, success: { response in
//            success(response)
        }, failure: { error in
//            failure(error)
        })
    }
    
    internal func configure(with configuration: [String : Any]) {
        if let batchCount = configuration[Visit.configKeys.batchSize] as? Int {
            batchSize = batchCount
        }
        
        if let batchTimeout = configuration[Visit.configKeys.batchTimeout] as? Double {
            batchTimeoutThreshold = batchTimeout // TODO: Make sure this is enough
        }
        
        if let sendBatchInBackground = configuration[Visit.configKeys.batchBackgroundSend] as? Bool {
            shouldSendInBackground = sendBatchInBackground
        }
    }
    
    internal func suspendBatch() {
        VSTFileUtility.suspendVNSBatch(locations: currentBatchLocations, beacons: currentBatchBeacons)
    }
    
    internal func reviveBatch() {
        let batch = VSTFileUtility.reviveVNSBatch()
        let launchLocations = VSTFileUtility.readLaunchLocations()
        VSTFileUtility.clearLaunchLocationRecords()
        VSTFileUtility.clearVNSBatch()
        currentBatchLocations = batch.0 + launchLocations
        currentBatchBeacons = batch.1
    }
}

extension VSTNetworkService: VSTDelegate {
    public func processBeacons(_ beacons: Set<VSTBeacon>) {
        currentBatchBeacons.append(contentsOf: beacons)
        currentBatchDidUpdate()
    }
    
    public func processLocation(_ location: CLLocation, with mode: VSTMovementMode) {
        currentBatchLocations.append(VSTCoreLocation(location))
        currentBatchDidUpdate()
    }
    
    public func movementModeDidChange(to: VSTMovementMode) {
        
    }
    
    public func locationUpdatesDidPause(with region: CLCircularRegion) {
        
    }
    
    public func locationUpdatesDidResume() {
        
    }
    
    public func updateStyleDidChange(to: VSTUpdateStyle) {
        
    }
    
    public func didSkipLocation(_ location: CLLocation) {
        
    }
    
    public func processTerminatedLocations(_ locations: [VSTCoreLocation]) {
        let batch = VSTFileUtility.reviveVNSBatch()
        VSTFileUtility.clearVNSBatch()
        processBatch(locations: batch.0 + locations, beacons: batch.1)
    }
    
    public func getConfiguration(success: @escaping ([String: Any]) -> Void, failure: @escaping (Error?) -> Void) {
        let params = getParams() as! VSTConfigParams
        if let location = CLLocationManager().location {
            params.location = VSTCoreLocation(location)
        }
        requestConfiguration(url: configurationURL, headers: headers, body: params, success: { configuration in
            success(configuration)
        }, failure: { error in
            failure(error)
        })
    }
}

// MARK: Debugging Functions
//    func checkBeaconForDuplicate(beacon: BeaconEvent) {
//        let beaconKey = "\(String(beacon.id))-\(String(beacon.major))-\(String(beacon.minor))"
//        if sentBeacons.contains(beaconKey) {
//            Visit.log("DUPLICATE BEACON")
//            if let count = duplicateBeacons[beaconKey] {
//                duplicateBeacons[beaconKey] = count + 1
//            } else {
//                duplicateBeacons[beaconKey] = 1
//            }
//        } else {
//            sentBeacons.insert(beaconKey)
//        }
//    }
//
//    private func analyzeJSON(object: Data) {
//        do {
//            let dict = try JSONSerialization.jsonObject(with: object, options: .allowFragments) as! [String: Any]
//            let formattedData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
//            let jsonString = String(data: formattedData, encoding: .utf8)
//            print(jsonString)
//            print("conversion success")
//        } catch {
//            print("conversion failure")
//        }
//    }

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIBackgroundTaskIdentifier(_ input: Int) -> UIBackgroundTaskIdentifier {
	return UIBackgroundTaskIdentifier(rawValue: input)
}
