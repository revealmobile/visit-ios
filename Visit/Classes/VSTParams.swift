//
//  VSTParams.swift
//  Visit
//
//  Created by Ben Roaman on 11/16/18.
//

import Foundation
import VSTCore

internal class VSTBaseParams: Encodable {
    var os: String?
    var deviceId: String?
    var appVersion: String?
    var sdkVersion: String?
    var appId: String?
    var timeZone: String?
    var locationSharingEnabled: Bool = false
    var locationPermissionRequested: String?
    var version: String?
    var locale: String?
    var conType: String?
    var idfa: String?
    var supportsBLE: Bool = false
    var manufacturer: String?
    var bluetoothEnabled: Bool = false
    var make: String?
    var model: String?
    var carrier: String?
    
    private enum CodingKeys: String, CodingKey {
        case os = "os"
        case deviceId = "device_id"
        case appVersion = "app_version"
        case sdkVersion = "sdk_version"
        case appId = "app_id"
        case timeZone = "time_zone"
        case locationSharingEnabled = "location_sharing_enabled"
        case locationPermissionRequested
        case version = "version"
        case locale = "locale"
        case conType = "con_type"
        case idfa
        case supportsBLE = "supports_ble"
        case manufacturer
        case bluetoothEnabled = "bluetooth_enabled"
        case make
        case model
        case carrier
    }
}

internal class VSTConfigParams: VSTBaseParams {
    var location: VSTCoreLocation?
    
    private enum CodingKeys: String, CodingKey {
        case location
    }
    
    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(location, forKey: .location)
        try super.encode(to: encoder)
    }
}

internal class VSTBatchParams: VSTBaseParams {
    var locations: [VSTBatchLocation] = []
    var beacons: [VSTBatchBeacon] = []
    
    private enum CodingKeys: String, CodingKey {
        case beacons
        case locations
    }
    
    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(locations, forKey: .locations)
        try container.encode(beacons, forKey: .beacons)
        try super.encode(to: encoder)
    }
}
