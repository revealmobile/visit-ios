//
//  VSTEventBatch.swift
//  Visit
//
//  Created by Ben Roaman on 1/12/18.
//

import Foundation
import VSTCore

internal class VSTEventBatch: Codable {
    public var beacons: [VSTBeacon] = []
    public var locations: [VSTCoreLocation] = []
    
    init(_ beacons: [VSTBeacon], _ locations: [VSTCoreLocation]) {
        self.beacons = beacons
        self.locations = locations
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        beacons = try values.decode([VSTBeacon].self, forKey: .beacons)
        locations = try values.decode([VSTCoreLocation].self, forKey: .locations)
    }
    
    private enum CodingKeys: String, CodingKey {
        case beacons
        case locations
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(beacons, forKey: .beacons)
        try container.encode(locations, forKey: .locations)
    }
}
