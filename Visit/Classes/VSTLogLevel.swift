//
//  VSTLogLevel.swift
//  CocoaLumberjack
//
//  Created by Ben Roaman on 8/15/19.
//

import Foundation

@objc public enum VSTLogLevel: Int {
    case debug = 0
    case location
    case beacon
    case info
    case error
    case undefined
}
