//
//  UIVisitDelegate.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import CoreLocation
import Visit
import MapKit
import CocoaLumberjack
import AdSupport

public class Annotation: NSObject, MKAnnotation {
    public var coordinate: CLLocationCoordinate2D
    public var color: UIColor
    public var title: String?
    
    public init(location: CLLocation, color: UIColor, title: String) {
        self.coordinate = location.coordinate
        self.color = color
        self.title = title
    }
}

public class Scannotation: Annotation {
    override public var hash: Int {
        var hasher = Hasher()
        hasher.combine(coordinate.latitude)
        hasher.combine(coordinate.longitude)
        return hasher.finalize()
    }
    
    override public func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? Scannotation else { return false }
        return coordinate.longitude == other.coordinate.longitude && coordinate.latitude == other.coordinate.latitude
    }
}

public typealias VisitLog = (message: String, level: VSTLogLevel, timestamp: String)

class UIVisitDelegate {
    // MARK: Constants
    public let vns: VSTNetworkService
    private let locationFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd - HH:mm:ss"
        return formatter
    }()
    
    private let logFormatter: DateFormatter = {
       let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/MM/dd - HH:mm:ss:SSS"
        return formatter
    }()
    
    // MARK: Instance Variables
    public private(set) var locations = [VSTMovementMode : [Annotation]]()
    public private(set) var beaconScans = [Scannotation : [VSTBeacon]]()
    
    public var timeStarted = Date()
    public private(set) var beaconTotal = 0
    public private(set) var pauseTotal = 0
    public private(set) var skippedLocationsTotal = 0
    
    public var allLocations: [Annotation] {
        var result = [Annotation]()
        for key in locations.keys {
            result.append(contentsOf: locations[key] ?? [])
        }
        return result
    }
    
    public private(set) var allLogs: [VisitLog] = []
    public private(set) var logs: [VSTLogLevel : [VisitLog]]
    
    // MARK: Initializer
    public init(vns: VSTNetworkService) {
        self.vns = vns
        self.locations = [.unknown : [], .walking : [], .city : [], .stationary : []]
        self.logs = [.error : [], .info : [], .debug: [], .location : [], .beacon : []]
    }
    
    // Mark: Data Conversion Functions
    private func convertLocationToAnnotation(_ location: CLLocation, mode: VSTMovementMode, title: String, skipped: Bool = false) -> Annotation {
        return Annotation(location: location, color: skipped ? getSkippedColor(for: mode) : getColor(for: mode), title: title)
    }
    
    private func convertLocationToScannotation(_ location: CLLocation, title: String) -> Scannotation {
        return Scannotation(location: location, color: .orange, title: title)
    }
    
    private func getCategory(for mode: VSTMovementMode) -> VSTMovementMode {
        switch mode {
        case .highway: return .city
        case .running: return .walking
        default: return mode
        }
    }
    
    private func getColor(for mode: VSTMovementMode) -> UIColor {
        switch mode {
        case .unknown: return .blue
        case .city: return .red
        case .highway: return .magenta
        case .walking: return .yellow
        case .running: return .orange
        case .stationary: return .green
        case .undefined: return .brown
        }
    }
    
    private func getSkippedColor(for mode: VSTMovementMode) -> UIColor {
        return getColor(for: mode).withAlphaComponent(0.4)
    }
}

// MARK: VSTDelegate Implementation
extension UIVisitDelegate: VSTDelegate {
    func getConfiguration(success: @escaping ([String : Any]) -> Void, failure: @escaping (Error?) -> Void) {
        vns.getConfiguration(success: success, failure: failure)
    }
    
    func processLocation(_ location: CLLocation, with mode: VSTMovementMode) {
        locations[getCategory(for: mode)]?.append(convertLocationToAnnotation(location, mode: mode, title: locationFormatter.string(from: location.timestamp)))
        vns.processLocation(location, with: mode)
    }
    
    func processBeacons(_ beacons: Set<VSTBeacon>, at location: CLLocation?) {
        beaconTotal += beacons.count
        if let scanLocation = location {
            let title = "\(beacons.count) Beacon\(beacons.count == 1 ? "" : "s")"
            let scannotation = convertLocationToScannotation(scanLocation, title: title)
            if let _ = beaconScans[scannotation] {
                beaconScans[scannotation]?.append(contentsOf: beacons)
            } else {
                beaconScans[scannotation] = Array(beacons)
            }
        }
        vns.processBeacons(beacons)
    }
    
    func locationUpdatesDidPause(with region: CLCircularRegion) {
        pauseTotal += 1
    }
    
    func didSkipLocation(_ location: CLLocation) {
        skippedLocationsTotal += 1
    }
    
    func shouldCreateLogs() -> Bool {
        return true
    }
    
    func didCreateLog(with message: String, at level: VSTLogLevel) {
        let log = (message, level, logFormatter.string(from: Date()))
        allLogs.append(log)
        logs[level]?.append(log)
        DDLogVerbose("{\"message\": \"\(message)\", \"idfa\": \"\(ASIdentifierManager.shared().advertisingIdentifier.uuidString)\" }")
    }
}
