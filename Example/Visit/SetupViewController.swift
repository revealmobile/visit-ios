//
//  ViewController.swift
//  Visit
//
//  Created by seanbdoherty on 11/16/2017.
//  Copyright (c) 2017 seanbdoherty. All rights reserved.
//

import UIKit
import Visit
import CoreLocation
import LogglyLogger_CocoaLumberjack

class SetupViewController: UIViewController {
    
    // MARK: Static Constants
    static let storyboardIdentifier = "SetupViewController"

    // MARK: Outlets
    @IBOutlet weak var versionNumberLabel: UILabel!
    @IBOutlet weak var serverSelectionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var locationPermissionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var locationPermissionLabel: UILabel!
    @IBOutlet weak var startOnBackgroundSegmentedControl: UISegmentedControl!
    @IBOutlet weak var debugLowEnergeySegmentedControl: UISegmentedControl!
    
    // MARK: Constants
    private let noUIViewControllerId = "VisitSDKIsCurrentlyRunning"
    
    // MARK: Actions
    @IBAction func startAction(_ sender: Any) {
        startFlow()
    }
    
    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways: locationPermissionLabel.text = "Current Permission: Always"
        case .authorizedWhenInUse: locationPermissionLabel.text = "Current Permission: While in Use"
        case .restricted: locationPermissionLabel.text = "Current Permission: Restricted"
        case .denied: locationPermissionLabel.text = "Current Permission: Denied"
        default: locationPermissionLabel.text = "Current Permission: Unknown"
        }
    }
    
    private func getBaseURL() -> String {
        switch serverSelectionSegmentedControl.selectedSegmentIndex {
        case 0:
            return "http://sandboxsdk.revealmobile.com/"
        case 1:
            return "https://sdk.revealmobile.com/"
        case 2:
            return "https://import.locarta.co/"
        default:
            return "http://sandboxsdk.revealmobile.com/"
        }
    }

    fileprivate func start() {
        // SETUP VNS
        let vns = (UIApplication.shared.delegate as! AppDelegate).vns
        let baseURL = getBaseURL()

        vns.setEventsURL(withString: baseURL + "api/v3/event/batch")
        vns.setConfigurationURL(withString: baseURL + "api/v3/info")
        
        var uiVisitDelegate: UIVisitDelegate?
        
        if debugLowEnergeySegmentedControl.selectedSegmentIndex == 0 {
            uiVisitDelegate = UIVisitDelegate(vns: vns)
            Visit.shared.setDelegate(uiVisitDelegate!)
            /// TODO: Visit.shared.logDelegate = LogManager.shared
            setupLoggly()
        } else {
            Visit.shared.setDelegate(vns)
        }
        uiVisitDelegate?.timeStarted = Date()
        if startOnBackgroundSegmentedControl.selectedSegmentIndex == 1 {
            DispatchQueue.global().async {
                Visit.shared.start(launchOptions: nil, onUserLaunch: nil)
            }
        } else {
            Visit.shared.canScanForIBeaconsAutomatically = false
            Visit.shared.canScanForBLEBeaconsAutomatically = false
            Visit.shared.canRequestBLEPermissionAutomatically = false
            Visit.shared.start(launchOptions: nil, onUserLaunch: nil)
        }
        if debugLowEnergeySegmentedControl.selectedSegmentIndex == 0 {
            let mainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: MainTabBarViewController.storyboardIdentifier) as! MainTabBarViewController
            mainTabBarController.uiVisitDelegate = uiVisitDelegate
            view.window?.rootViewController = mainTabBarController
        } else {
            view.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: noUIViewControllerId)
        }
    }
    
    private func startFlow() {
        if debugLowEnergeySegmentedControl.selectedSegmentIndex == 0 {
//            BuddyBuildSDK.setup()
        }
        switch locationPermissionSegmentedControl.selectedSegmentIndex {
        case 0:
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse, .denied:
                start()
            default:
                LocationHelper.shared.manager.delegate = self
                LocationHelper.shared.manager.requestAlwaysAuthorization()
            }
        case 1:
            start()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5.0) {
                LocationHelper.shared.manager.requestAlwaysAuthorization()
            }
        case 2:
            start()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 10.0) {
                LocationHelper.shared.manager.requestAlwaysAuthorization()
            }
        case 3:
            start()
        default:
            return
        }
    }
    
    private func setupLoggly() {
        let logger = LogglyLogger()
        logger.logFormatter = LogglyFormatter()
        logger.logglyKey = "c9ef746c-d17d-480a-89c8-8df52ed64bd6"
        logger.saveInterval = 15.0
        DDLog.add(logger)
    }

}

extension SetupViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.start()
        manager.delegate = nil
    }
}

