//
//  LogTableViewCell.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class LogTableViewCell: UITableViewCell {
    // MARK: Static Constants
    public static let reuseId = "LogTableViewCell"
    
    // MARK: Outlets
    @IBOutlet weak var categoryIconImageView: UIImageView!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
}
