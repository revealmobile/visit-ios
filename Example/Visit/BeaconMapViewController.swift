//
//  BeaconMapViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import MapKit
import Visit

class BeaconMapViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: Actions
    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(Array(visitDelegate!.beaconScans.keys))
    }
    
    @IBAction func centerAction(_ sender: UIBarButtonItem) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.showAnnotations(Array(visitDelegate!.beaconScans.keys), animated: true)
    }
    
    @IBAction func scanAction(_ sender: Any) {
        let alert = UIAlertController(title: "Execute Manual Scan", message: "What kind of beacons scan would you like to perform?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Full", style: .default, handler: { action in
            Visit.shared.executeFullBeaconScan()
        }))
        alert.addAction(UIAlertAction(title: "iBeacon", style: .default, handler: { action in
//            Visit.shared.executeIBeaconScan()
        }))
        alert.addAction(UIAlertAction(title: "Non-iBeacon", style: .default, handler: { action in
//            Visit.shared.executeBLEBeaconScan()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Instance Variables
    private var visitDelegate: UIVisitDelegate?
    
    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        visitDelegate = (navigationController?.tabBarController as? MainTabBarViewController)?.uiVisitDelegate
        mapView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for annotation in mapView.selectedAnnotations {
            mapView.deselectAnnotation(annotation, animated: true)
        }
    }
}

extension BeaconMapViewController: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        let pin = MKPinAnnotationView()
//        pin.pinTintColor = (annotation as? Annotation)?.color ?? UIColor.cyan
//        pin.canShowCallout = true
//        pin.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        return pin
//    }
    
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
//        guard let annotation = view.annotation as? Annotation, let beacons = visitDelegate?.beaconScans[annotation] else {
//            return
//        }
//        
//        guard let detailView = UIStoryboard(name: "Beacons", bundle: nil).instantiateViewController(withIdentifier: BeaconListViewController.storyboardId) as? BeaconListViewController else {
//            return
//        }
//        
//        detailView.annotation = annotation
//        detailView.beacons = beacons
//        navigationController?.pushViewController(detailView, animated: true)
//    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? Scannotation, let beacons = visitDelegate?.beaconScans[annotation] else {
            return
        }
        
        guard let detailView = UIStoryboard(name: "Beacons", bundle: nil).instantiateViewController(withIdentifier: BeaconListViewController.storyboardId) as? BeaconListViewController else {
            return
        }
        
        detailView.annotation = annotation
        detailView.beacons = beacons
        navigationController?.pushViewController(detailView, animated: true)
    }
}
