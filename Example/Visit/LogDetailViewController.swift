//
//  LogDetailViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/26/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class LogDetailViewController: UIViewController {
    // MARK: Static Constants
    public static let storyboardId = "LogDetailViewController"
    
    // MARK: Instance Variables
    public var log: VisitLog?
    
    // MARK: Outlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    

    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let log = log else { return }
        iconImageView.image = UIImage.logImage(for: log.level)
        let color = UIColor.logColor(for: log.level)
        iconImageView.tintColor = color
        timestampLabel.textColor = color
        timestampLabel.text = log.timestamp
        messageLabel.text = log.message
    }
}
