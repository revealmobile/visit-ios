 //
//  AppDelegate.swift
//  Visit
//
//  Created by seanbdoherty on 11/16/2017.
//  Copyright (c) 2017 seanbdoherty. All rights reserved.
//

import UIKit
import Visit
import UserNotifications
import CocoaLumberjack
import LogglyLogger_CocoaLumberjack
import CoreLocation

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    public let vns = VSTNetworkService(apiKey: "517D85EAB6AA3F7453EEBFBDC4C25F4DCA539B5EA28EA6DDFC91A1")
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        if !(application.currentUserNotificationSettings?.types.contains([.alert, .sound, .badge]) ?? false) {
//            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
//        }

        vns.setHeader(key: "Content-Type", value: "application/json")
        
        if let _ = launchOptions?[UIApplication.LaunchOptionsKey.location] {
            vns.setEventsURL(withString: "https://sdk.revealmobile.com/api/v3/event/batch")
            Visit.shared.setDelegate(vns)
            Visit.shared.startsOnTrueLaunch = false
            Visit.shared.start(launchOptions: launchOptions, onUserLaunch: { options in
                LocationHelper.shared.activate()
            })
        } else {
            LocationHelper.shared.activate()
        }
        
        return true
    }
    
//    private func createNotification(_ message: String, application: UIApplication) {
//        let notification = UILocalNotification()
//        notification.alertBody = message
//        notification.fireDate = Date().addingTimeInterval(10)
//        application.scheduleLocalNotification(notification)
//    }
}
