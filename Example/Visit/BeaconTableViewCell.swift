//
//  BeaconTableViewCell.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class BeaconTableViewCell: UITableViewCell {
    // MARK: Static Constants
    public static let reuseId = "BeaconTableViewCell"
    
    // MARK: Outlets
    @IBOutlet weak var vendorLabel: UILabel!
    @IBOutlet weak var proximityLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var minorLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
}
