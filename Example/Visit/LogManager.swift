////
////  LogManager.swift
////  Visit_Example
////
////  Created by Ben Roaman on 1/10/18.
////  Copyright © 2018 CocoaPods. All rights reserved.
////
//
//import Foundation
//import Visit
//import CocoaLumberjack
//import AdSupport
//
//class LogManager: NSObject {
//    
//    // MARK: Singleton
//    
//    public static let shared = LogManager()
//    
//    // MARK: Instance Variables
//    
//    fileprivate var logs: [Log] = []
//    
//    // MARK: Constants
//    
//    fileprivate let dateFormatter: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss:SSS"
//        return formatter
//    }()
//
//    // MARK: Static Constants
//    
//    public static let logManagerReceivedLogNotification = "com.visit.logmanager.notification.logmanagerreceivedlog"
//    
//    // MARK: Getters
//    
//    public func getLogs() -> [Log] {
//        return logs
//    }
//    
//    // MARK: Log Data Class
//    
//    public class Log: NSObject {
//        public var message: String! = ""
//        public var timestamp: String! = ""
//        init(_ message: String, _ timestamp: String) {
//            super.init()
//            self.message = message
//            self.timestamp = timestamp
//        }
//    }
//}
//
//// MARK: VisitLogDelegate Implementation
//
//extension LogManager: VSTLogDelegate {
//    func didCreateLog(withItems items: Any..., atLevel level: Visit.LogLevel) {
//        var message = ""
//        for (index, item) in items.enumerated() {
//            message += "\(item as? String ??  String(describing: item as? Error ?? item))"
//            if index != items.count - 1 { message += ", "}
//        }
//        logs.insert(Log(message, dateFormatter.string(from: Date())), at: 0)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: LogManager.logManagerReceivedLogNotification), object: nil)
//        DDLogVerbose("{\"message\": \"\(message)\", \"idfa\": \"\(ASIdentifierManager.shared().advertisingIdentifier.uuidString)\" }")
//    }
//}
