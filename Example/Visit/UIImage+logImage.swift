//
//  UIImage+Icons.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/21/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import Visit

extension UIImage {
    public static func logImage(for level: VSTLogLevel) -> UIImage {
        switch level {
        case .debug: return logIconDebug
        case .error: return logIconError
        case .info: return logIconInfo
        case .beacon: return logIconBeacon
        case .location: return logIconLocation
        case .undefined: return UIImage()
        }
    }
    
    public static let logIconDebug = UIImage(named: "ic-28-debug")!
    public static let logIconError = UIImage(named: "ic-28-error")!
    public static let logIconInfo = UIImage(named: "ic-28-info")!
    public static let logIconBeacon = UIImage(named: "ic-28-beacon")!
    public static let logIconLocation = UIImage(named: "ic-28-location")!
}
