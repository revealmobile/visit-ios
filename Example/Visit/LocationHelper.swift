//
//  LocationHelper.swift
//  Visit_Example
//
//  Created by Ben Roaman on 1/15/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Visit
import UIKit
import MapKit

class LocationHelper: NSObject {
    // MARK: Singleton
    static let shared = LocationHelper()
    
    // MARK: Static Constants
    static let locationHelperGotLocationsNotification = "com.visit.notifications.locationhelpergotlocations"
    
    // MARK: Constants
    let manager = CLLocationManager()
    
    // MARK: Instance Variables
    private var annotations: [Annotation] = []
    
    // MARK: Getters and Setters
    func getAnnotations() -> [Annotation] {
        return annotations
    }
    
//    func convertAndAddStorables(storables: [Storable]) {
//        let newAnnotations = convertStorablesToAnnotations(storables: storables)
//        annotations.append(contentsOf: newAnnotations)
//        NotificationCenter.default.post(name: Notification.Name(rawValue: LocationHelper.locationHelperGotLocationsNotification), object: newAnnotations)
//    }
//
//    private func convertStorablesToAnnotations(storables: [Storable]) -> [Annotation] {
//        var annotations = [Annotation]()
//        for storable in storables {
//            if let location = storable as? LocationEvent {
//                annotations.append(Annotation(coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude), color: UIColor.red))
//            } else if let beacon = storable as? BeaconEvent, let coordinate = beacon.location?.coordinate {
//                annotations.append(Annotation(coordinate: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude) , color: UIColor.blue))
//            }
//        }
//        return annotations
//    }
    
    
    public func activate() {
        
    }
}
