//
//  NotificationTester.swift
//  Visit_Example
//
//  Created by Ben Roaman on 4/11/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

//class NotificationTester: NSObject {
//    
//    static let shared = NotificationTester()
//    
//    override private init() {
//        super.init()
//        NotificationCenter.default.addObserver(self, selector: #selector(self.respondToLaunchNotification(_:)), name: .UIApplicationDidFinishLaunching, object: nil)
//    }
//    
//    func activate() {}
//    
//    @objc private func respondToLaunchNotification(_ notification: Notification) {
//        var message = "UIApplicationDidFinishLaunching, UserInfo Keys: "
//        if let userInfo = notification.userInfo {
//            for key in userInfo.keys {
//                message += "\(key), "
//            }
//        }
//        createLocalPush(message)
//    }
//    
//    private func createLocalPush(_ message: String) {
//        let notification = UILocalNotification()
//        notification.alertBody = message
//        notification.fireDate = Date().addingTimeInterval(5)
//        UIApplication.shared.scheduleLocalNotification(notification)
//    }
//}

