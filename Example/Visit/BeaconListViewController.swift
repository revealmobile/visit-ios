//
//  BeaconListViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import Visit
import CoreLocation

class BeaconListViewController: UIViewController {
    // MARK: Static Constants
    public static let storyboardId = "BeaconListViewController"
    
    // MARK: Outlets
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    @IBOutlet weak var beaconTableView: UITableView!
    
    // MARK: Instance Variables
    public var beacons: [VSTBeacon] = []
    public var annotation: Annotation?
    
    // MARK: Constants
    // MARK: Private Constants
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/MM/dd - HH:mm:ss:SSS"
        return formatter
    }()
    
    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        beaconTableView.dataSource = self
        beaconTableView.delegate = self
        beaconTableView.reloadData()
        latLabel.text = "\(annotation?.coordinate.latitude ?? -1)"
        lonLabel.text = "\(annotation?.coordinate.longitude ?? -1)"
    }
    
    // MARK: Private UI Functions {
    private func getProximityString(for proximity: CLProximity) -> String {
        switch proximity {
        case .unknown: return "Unknown"
        case .far: return "Far"
        case .near: return "Near"
        case .immediate: return "Immediate"
        }
    }
}

extension BeaconListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beacons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BeaconTableViewCell.reuseId) as? BeaconTableViewCell else {
            return UITableViewCell()
        }
        let beacon = beacons[indexPath.row]
        cell.vendorLabel.text = beacon.vendorName
        cell.proximityLabel.text = getProximityString(for: beacon.proximity)
        cell.rssiLabel.text = "\(beacon.rssi)"
        cell.uuidLabel.text = beacon.proximityUUID
        cell.majorLabel.text = beacon.major.isEmpty ? "N/A" : beacon.major
        cell.minorLabel.text = beacon.minor.isEmpty ? "N/A" : beacon.minor
        cell.timestampLabel.text = formatter.string(from: beacon.createdAt)
        return cell
    }
}

extension BeaconListViewController: UITableViewDelegate {
    
}
