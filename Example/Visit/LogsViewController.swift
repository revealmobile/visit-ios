//
//  LogsViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import Visit

class LogsViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var levelSegmentedControl: UISegmentedControl!
    @IBOutlet weak var logsTableView: UITableView!
    @IBOutlet weak var zeroItemLabel: UILabel!
    
    // MARK: Actions
    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        refreshLogs()
    }
    
    @IBAction func topAction(_ sender: UIBarButtonItem) {
        logsTableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
    }
    
    @IBAction func bottomAction(_ sender: UIBarButtonItem) {
        let scrollPoint = CGPoint(x: 0, y: logsTableView.contentSize.height - logsTableView.frame.size.height)
        logsTableView.setContentOffset(scrollPoint, animated: false)
    }
    
    @IBAction func changeLevelAction(_ sender: UISegmentedControl) {
        refreshLogs()
    }
    // MARK: Instance Variables
    private var visitDelegate: UIVisitDelegate?
    private var currentLogs: [VisitLog] = []
    
    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        visitDelegate = (navigationController?.tabBarController as? MainTabBarViewController)?.uiVisitDelegate
        logsTableView.dataSource = self
        logsTableView.delegate = self
    }
    
    // MARK: Private Data Functions
    private func refreshLogs() {
        switch levelSegmentedControl.selectedSegmentIndex {
        case 0: currentLogs = visitDelegate?.allLogs ?? []
        case 1: currentLogs = visitDelegate?.logs[.debug] ?? []
        case 2: currentLogs = visitDelegate?.logs[.location] ?? []
        case 3: currentLogs = visitDelegate?.logs[.beacon] ?? []
        case 4: currentLogs = visitDelegate?.logs[.error] ?? []
        default: currentLogs = visitDelegate?.logs[.info] ?? []
        }
        logsTableView.reloadData()
    }
}

// MARK: UITableViewDataSource Implementation
extension LogsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if currentLogs.count > 0 {
            tableView.separatorStyle = .singleLine
            zeroItemLabel.isHidden = true
            return 1
        } else {
            tableView.separatorStyle = .none
            zeroItemLabel.isHidden = false
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentLogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LogTableViewCell.reuseId) as? LogTableViewCell else {
            return UITableViewCell()
        }
        let log = currentLogs[indexPath.row]
        cell.categoryIconImageView.image = UIImage.logImage(for: log.level)
        cell.categoryIconImageView.tintColor = UIColor.logColor(for: log.level)
        cell.timestampLabel.text = log.timestamp
        cell.messageLabel.text = log.message
        return cell
    }
}

// MARK: UITableViewDelegate Implementation
extension LogsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailController = UIStoryboard(name: "Logs", bundle: nil).instantiateViewController(withIdentifier: LogDetailViewController.storyboardId) as? LogDetailViewController else {
            return
        }
        detailController.log = currentLogs[indexPath.row]
        navigationController?.pushViewController(detailController, animated: true)
    }
}
