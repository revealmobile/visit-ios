//
//  LocationMapViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import MapKit

class LocationMapViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var modeSegmentedControl: UISegmentedControl!
    
    // MARK: Actions
    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        refreshAnnotations()
    }
    
    @IBAction func centerAction(_ sender: UIBarButtonItem) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.showAnnotations(currentAnnotations, animated: true)
    }
    
    @IBAction func modeChangeAction(_ sender: UISegmentedControl) {
        refreshAnnotations()
    }
    
    // MARK: Instance Variables
    private var visitDelegate: UIVisitDelegate?
    private var currentAnnotations: [Annotation] = []
    
    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        visitDelegate = (navigationController?.tabBarController as? MainTabBarViewController)?.uiVisitDelegate
        mapView.delegate = self
    }
    
    private func refreshAnnotations(centered: Bool = false) {
        switch modeSegmentedControl.selectedSegmentIndex {
        case 0: currentAnnotations = visitDelegate?.allLocations ?? []
        case 1: currentAnnotations = visitDelegate?.locations[.walking] ?? []
        case 2: currentAnnotations = visitDelegate?.locations[.city] ?? []
        case 3: currentAnnotations = visitDelegate?.locations[.stationary] ?? []
        default: currentAnnotations = visitDelegate?.locations[.unknown] ?? []
        }
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(currentAnnotations)
    }
}

extension LocationMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            let pin = MKPinAnnotationView()
            pin.pinTintColor = (annotation as? Annotation)?.color ?? UIColor.cyan
            pin.canShowCallout = true
            return pin
    }
}
