//
//  StatsTableViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import Visit

class StatsTableViewController: UITableViewController {
    // MARK: Constants
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYY.MM.dd - HH:mm:ss"
        return formatter
    }()
    
    // MARK: Outlets
    @IBOutlet weak var startedLLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var locationCountLabel: UILabel!
    @IBOutlet weak var skippedLocationCountLabel: UILabel!
    @IBOutlet weak var beaconCountLabel: UILabel!
    @IBOutlet weak var pauseCountLabel: UILabel!
    
    // MARK: Actions
    @IBAction func refreshAction(_ sender: UIBarButtonItem) {
        setUI()
    }
    
    // MARK: Instance Variables
    private var visitDelegate: UIVisitDelegate?
    
    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        visitDelegate = (navigationController?.tabBarController as? MainTabBarViewController)?.uiVisitDelegate
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUI()
    }
    
    // MARK: UI Functions
    private func setUI() {
        startedLLabel.text = formatter.string(from: visitDelegate?.timeStarted ?? Date())
        stateLabel.text = Visit.shared.currentUpdateState.name
        modeLabel.text = Visit.shared.currentMovementMode.name
        styleLabel.text = Visit.shared.currentUpdateStyle.name
        locationCountLabel.text = "\(visitDelegate?.allLocations.count ?? 0)"
        skippedLocationCountLabel.text = "\(visitDelegate?.skippedLocationsTotal ?? 0)"
        beaconCountLabel.text = "\(visitDelegate?.beaconTotal ?? 0)"
        pauseCountLabel.text = "\(visitDelegate?.pauseTotal ?? 0)"
    }
}
