//
//  UIColor+logColor.swift
//  Visit_Example
//
//  Created by Ben Roaman on 11/26/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import Visit

extension UIColor {
    public static func logColor(for level: VSTLogLevel) -> UIColor {
        switch level {
        case .debug: return logColorDebug
        case .error: return logColorError
        case .info: return logColorInfo
        case .beacon: return logColorBeacon
        case .location: return logColorLocation
        case .undefined: return .clear
        }
    }
    
    public static let logColorError = UIColor(red: 178/255, green: 34/255, blue: 34/255, alpha: 1) // HTML FireBrick #B22222
    public static let logColorBeacon = UIColor(red: 65/255, green: 105/255, blue: 225/255, alpha: 1) // HTML RoyalBlue #4169E1
    public static let logColorLocation = UIColor(red: 46/255, green: 139/255, blue: 87/255, alpha: 1) // HTML RebeccaPurple #663399
    public static let logColorInfo = UIColor(red: 1, green: 140/255, blue: 0, alpha: 1) // HTML DarkOrange #FF8C00
    public static let logColorDebug = UIColor(red: 139/255, green: 69/255, blue: 19/255, alpha: 1) // HTML SaddleBrown #8B4513
}
