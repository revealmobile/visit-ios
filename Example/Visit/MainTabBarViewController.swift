//
//  MainTabBarViewController.swift
//  Visit_Example
//
//  Created by Ben Roaman on 1/10/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import Visit

class MainTabBarViewController: UITabBarController {
    // MARK: Static Constants
    static let storyboardIdentifier = "MainTabBarViewController"

    // MARK: Instance Variables
    public var uiVisitDelegate: UIVisitDelegate?
}
